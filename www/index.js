import { Universe, Cell } from "wasm-game-of-life";
import { memory } from "wasm-game-of-life/wasm_game_of_life_bg";

// config settings
const CELL_SCALE    = 4;
const CELL_SIZE     = CELL_SCALE+1;
const GRID_COLOR    = "#CCCCCC";
const DEAD_COLOR    = "#FFFFFF";
const ALIVE_COLOR   = "#FF8800";

// universe object
const universe  = Universe.new(64, 64);
const height    = universe.get_height();
const width     = universe.get_width();

// play / pause button
const pause_button = document.getElementById("toggle-pause");

// canvas to render to
const canvas       = document.getElementById("game-of-life-canvas");
canvas.height      = CELL_SIZE*height+1;
canvas.width       = CELL_SIZE*width+1;
const ctx          = canvas.getContext('2d');
let animation_id = null;

const isPaused = () =>
{
    return animation_id === null;
}

const play = () => 
{
    pause_button.textContent = "PAUSE";
    gameLoop();
}

const pause = () =>
{
    pause_button.textContent = "PLAY";
    cancelAnimationFrame(animation_id);
    animation_id = null;
}

const drawGrid = () =>
{
    ctx.beginPath();
    ctx.strokeStyle = GRID_COLOR;

    // vertical lines
    for (let x = 0; x <= width; x++)
    {
        ctx.moveTo(x*CELL_SIZE+1, 0);
        ctx.lineTo(x*CELL_SIZE+1, CELL_SIZE*height+1);
    }

    // horizontal lines
    for (let y = 0; y <= height; y++)
    {
        ctx.moveTo(0,                 y*CELL_SIZE+1);
        ctx.lineTo(CELL_SIZE*width+1, y*CELL_SIZE+1);
    }

    ctx.stroke();
}

const getIndex = (x, y) =>
{
    return y*width+x;
};

const checkBit = (n, arr) =>
{
    const byte = Math.floor(n/8);
    const mask = 1 << (n%8);
    return (arr[byte] & mask) === mask;
};

const drawCells = () =>
{
    const cells = new Uint8Array(
        memory.buffer,
        universe.get_cells(),
        width*height/8 // each cell is a bit, so 8 cells per byte
    );

    ctx.beginPath();

    for (let y = 0; y < height; y++)
    {
        for (let x = 0; x < width; x++)
        {
            const index = getIndex(x, y);
            ctx.fillStyle = checkBit(index, cells)
                ? ALIVE_COLOR
                : DEAD_COLOR;
            ctx.fillRect(
                x*CELL_SIZE+1,
                y*CELL_SIZE+1,
                CELL_SCALE,
                CELL_SCALE
            );
        }
    }

    ctx.stroke();
};

const render = () =>
{
    drawGrid();
    drawCells();
};

const gameLoop = () =>
{
    // processing 1 tick
    universe.tick();
    
    // rendering to canvas
    render();

    animation_id = requestAnimationFrame(gameLoop);
};


// event handling
pause_button.addEventListener("click", event =>
    {
        if(isPaused())
        {
            play();
        }
        else
        {
            pause();
        }
    });

canvas.addEventListener("click", event =>
    {
        const bounds = canvas.getBoundingClientRect();
        const x_scale = canvas.width / bounds.width;
        const y_scale = canvas.height / bounds.height;

        const left = (event.clientX - bounds.left) * x_scale;
        const top = (event.clientY - bounds.top) * y_scale;

        const x = Math.min(Math.floor(left / CELL_SIZE), width - 1);
        const y = Math.min(Math.floor(top / CELL_SIZE), height - 1);

        universe.toggle_cell(x, y);

        render();
    });

play();

