mod utils;

use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

use std::mem;
use std::fmt;
use fixedbitset::FixedBitSet;

macro_rules! log 
{
    ($($t:tt)*) =>
    {
        web_sys::console::log_1(&format!($($t)*).into());      
    }
}

#[wasm_bindgen]
#[repr(u8)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Cell
{
    DEAD    = 0,
    ALIVE   = 1,
}

impl Cell
{
    fn toggle(&mut self)
    {
        *self = match *self
        {
            Cell::DEAD => Cell::ALIVE,
            Cell::ALIVE => Cell::DEAD
        };
    }
}

#[wasm_bindgen]
pub struct Universe
{
    width: u32,
    height: u32,
    cells: FixedBitSet,
}

// JS exposed functions
#[wasm_bindgen]
impl Universe
{
    pub fn new(width: u32, height: u32) -> Self
    {
        utils::set_panic_hook();

        let size = (width*height) as usize;
        let cells = FixedBitSet::with_capacity(size);
        
        let mut universe = Universe
        {
            width,
            height,
            cells
        };
        universe.gen_cells();
        log!("universe created");

        return universe;
    }

    pub fn tick(&mut self)
    {
        let mut next = self.cells.clone(); 
        for y in 0..self.height
        {
            for x in 0..self.width
            {
                let index = self.get_index(x, y);
                let neighbors = self.get_cell_neighbors(x, y);
                let original_value = self.cells[index];
                let value = match neighbors
                {
                    2 => original_value,
                    3 => true,
                    _ => false
                };
                
                /*
                if value != original_value
                {
                    log!("cell at ({}, {}) was initially {} and is now {} becuase it had {} neighbors",
                        x, y,
                        if original_value { "alive" } else { "dead" },
                        if value { "alive" } else { "dead" },
                        neighbors
                    );
                }
                */

                next.set(index, value);
            }
        }

        mem::swap(&mut self.cells, &mut next);
    }

    pub fn get_width(&self) -> u32
    {
        self.width
    }

    pub fn get_height(&self) -> u32
    {
        self.height
    }

    pub fn get_cells(&self) -> *const u32
    {
        self.cells.as_slice().as_ptr()
    }

    pub fn set_width(&mut self, width: u32)
    {
        self.width = width;
        self.gen_cells();
    }

    pub fn set_height(&mut self, height: u32)
    {
        self.height = height;
        self.gen_cells();
    }
    
    pub fn toggle_cell(&mut self, x: u32, y: u32)
    {
        let index = self.get_index(x, y);
        self.cells.set(index, !self.cells[index]);
    }
}

// non JS-exposed functions
impl Universe
{
    fn get_index(&self, x: u32, y: u32) -> usize
    {
        (self.width*y + x) as usize
    }

    fn get_cell_neighbors(&self, x: u32, y: u32) -> u8
    {
        let mut sum = 0;
        for dy in [self.height-1, 0, 1].iter().cloned()
        {
            for dx in [self.width-1, 0, 1].iter().cloned()
            {
                if dx == 0 && dy == 0
                {
                    continue;
                }

                let x_pos = (x + dx) % self.width;
                let y_pos = (y + dy) % self.height;
                let index = self.get_index(x_pos, y_pos);
                
                sum += self.cells[index] as u8;
            }
        }

        return sum;
    }


    fn gen_cells(&mut self)
    {
        for i in 0..(self.width*self.height) as usize
        {
            self.cells.set(i, i%2==0 || i%5==0);
        }
    }
}

